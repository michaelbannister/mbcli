package selfupdate

import (
	"bufio"
	"fmt"
	"github.com/mouuff/go-rocket-update/pkg/updater"
	"log"
	"mbcli/internal/selfupdate/provider"
	"os"
	"runtime"
	"strings"
)

func CheckAndPromptForUpdate(currentVersion string) {
	if strings.Contains(currentVersion, "SNAPSHOT") {
		_, _ = fmt.Fprintln(os.Stderr, "Snapshot version; skipping update check…")
		return
	}

	u := &updater.Updater{
		Provider:       provider.NewGcsProvider("mbcli-releases", "mbcli/"),
		ExecutableName: fmt.Sprintf("mbcli_%s_%s", runtime.GOOS, runtime.GOARCH),
		Version:        currentVersion,
	}
	canUpdate, err := u.CanUpdate()
	if err != nil {
		log.Fatal(err)
	}
	if !canUpdate {
		return
	}
	latestVersion, err := u.GetLatestVersion()
	if err != nil {
		log.Fatal(err)
	}

	msg := fmt.Sprintf("New version available: %s (current is %s). Update now?", latestVersion, currentVersion)

	doUpdate := yesNoPrompt(msg, true)
	if doUpdate {
		status, err := u.Update()
		if err != nil {
			log.Fatal(err)
		}
		if status != updater.Updated {
			log.Fatal("Failed to update")
		}
	}

}

func yesNoPrompt(label string, def bool) bool {
	choices := "Y/n"
	if !def {
		choices = "y/N"
	}

	r := bufio.NewReader(os.Stdin)
	var s string

	for {
		fmt.Fprintf(os.Stderr, "%s (%s) ", label, choices)
		s, _ = r.ReadString('\n')
		s = strings.TrimSpace(s)
		if s == "" {
			return def
		}
		s = strings.ToLower(s)
		if s == "y" || s == "yes" {
			return true
		}
		if s == "n" || s == "no" {
			return false
		}
	}
}
