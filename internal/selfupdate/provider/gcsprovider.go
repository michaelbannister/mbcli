package provider

import (
	"cloud.google.com/go/storage"
	"context"
	"errors"
	"github.com/mouuff/go-rocket-update/pkg/provider"
	"google.golang.org/api/iterator"
	"log"
	"strings"
)

type GcsProvider struct {
	bucket *storage.BucketHandle
	Path   string
}

func NewGcsProvider(bucketName, path string) *GcsProvider {
	client, err := storage.NewClient(context.Background())
	if err != nil {
		// TODO improve this - could happen if no internet access?
		log.Fatal(err)
	}
	return &GcsProvider{
		bucket: client.Bucket(bucketName),
		Path:   path,
	}
}

func (c *GcsProvider) GetLatestVersion() (string, error) {
	ctx := context.Background()
	objs := c.bucket.Objects(ctx, &storage.Query{
		Prefix:                   c.Path,
		Delimiter:                "/",
		StartOffset:              c.Path + "0.0.1/",
		IncludeFoldersAsPrefixes: true,
	})

	var latestVersion string
	for {
		attrs, err := objs.Next()
		if errors.Is(err, iterator.Done) {
			break
		}
		if err != nil {
			return "", err
		}
		pathParts := strings.Split(attrs.Prefix, "/")
		latestVersion = pathParts[len(pathParts)-2]
	}
	return latestVersion, nil
}

func (c *GcsProvider) Walk(walkFn provider.WalkFunc) error {
	//TODO implement me
	panic("implement me")
}

func (c *GcsProvider) Retrieve(srcPath string, destPath string) error {
	//TODO implement me
	panic("implement me")
}

func (c *GcsProvider) Open() error {
	//TODO implement me
	panic("implement me")
}

func (c *GcsProvider) Close() error {
	//TODO implement me
	panic("implement me")
}
