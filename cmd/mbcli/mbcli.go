package main

import "mbcli/internal/selfupdate"

var (
	version string // set by default by goreleaser
)

func main() {
	println("Hello!")

	selfupdate.CheckAndPromptForUpdate(version)

}
